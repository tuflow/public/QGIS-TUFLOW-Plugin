location == ESTRY Control File (.ecf)

command == Read GIS WLL

description == Used to translate 1D results into 2D map output for viewing in SMS, WaterRide, Blue Kenu and GIS Software.

wiki link == https://wiki.tuflow.com/TUFLOW_Empty_Files#ESTRY_Control_file_.28ECF.29

manual link == https://docs.tuflow.com/classic-hpc/manual/2023-03/Outputs-1.html#OneDMap-3