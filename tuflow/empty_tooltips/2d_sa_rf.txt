location == BC Control File (.tbc)

command == Read GIS SA RF

description == Used to define the location of internal source area flow verse time boundaries. The model input is specified using rainfall hyetographs (mm versus hours) instead of flow hydrographs (flow verse time).

wiki link == https://wiki.tuflow.com/TUFLOW_Empty_Files#TUFLOW_Boundary_Control_file_.28TBC.29

manual link == https://docs.tuflow.com/classic-hpc/manual/2023-03/Boundaries-InitialConditions-1.html#tab:tab-SA-RF