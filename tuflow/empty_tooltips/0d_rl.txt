location == TUFLOW Control File (.tcf)

command == Read GIS Reporting Location

description == This file contains lines and points defining Reporting Location(s) (RL) for 1D and 2D result time series graphs in Excel.

wiki link == https://wiki.tuflow.com/TUFLOW_Empty_Files#TUFLOW_Control_file_.28TCF.29

manual link == https://docs.tuflow.com/classic-hpc/manual/2023-03/Outputs-1.html#ReportingLocation-3